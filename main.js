var div_canvas = document.getElementById("div_canvas");
var canvas = document.getElementById("myCanvas");

//html color picker
var color = "Black";
document.getElementById("html_color").addEventListener("change", function(){
    color = document.getElementById("html_color").value;
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
});

//line width(brush size)
var slider_brushSize = document.getElementById("brush_size");
slider_brushSize.addEventListener("change", function(){
    ctx.lineWidth = slider_brushSize.value;
});

//tools init
var btn_brush = document.getElementById("brush");
var btn_eraser = document.getElementById("eraser");
var btn_text = document.getElementById("text");
var btn_circle = document.getElementById("circle");
var btn_rect = document.getElementById("rect");
var btn_triangle = document.getElementById("triangle");
var btn_download = document.getElementById("download");
var btn_upload = document.getElementById("upload");
var btn_undo = document.getElementById("undo");
var btn_redo = document.getElementById("redo");
var btn_reset = document.getElementById("reset");

btn_brush.addEventListener("click", select_tool);
btn_eraser.addEventListener("click", select_tool);
btn_text.addEventListener("click", select_tool);
btn_circle.addEventListener("click", select_tool);
btn_rect.addEventListener("click", select_tool);
btn_triangle.addEventListener("click", select_tool);
var tool_selected = "brush";
function select_tool(){
    tool_selected = this.id;

    if(text.ready){
        text_finish();
    }

    switch(tool_selected){
        case "brush":
        ctx.strokeStyle = color;
        ctx.lineWidth = slider_brushSize.value;
        canvas.style.cursor = "url('./brush.png') 0 64, default";
        break;
        case "eraser":
        ctx.strokeStyle = "White";
        ctx.lineWidth = slider_brushSize.value;
        canvas.style.cursor = "url('./eraser.png') 16 64, default";
        break;
        case "text":
        canvas.style.cursor = "url('./txt.png') 0 64, default";
        break;
        case "circle":
        case "rect":
        case "triangle":
        canvas.style.cursor = "url('./plus-symbol.png') 32 32, default";
        break;
        default:
        canvas.style.cursor = "default";
        break;
    }
    console.log(tool_selected);
}
canvas.style.cursor = "url('./brush.png') 0 64, default";

var isDragged = false;
canvas.addEventListener("mousedown", function(MouseEvent){
    isDragged = true;
    mouse.start_x = MouseEvent.offsetX;
    mouse.start_y = MouseEvent.offsetY;
    if(tool_selected == "text"){
        if(!text.ready){
            text.x = MouseEvent.offsetX;
            text.y = MouseEvent.offsetY;
            drawText();
        }
        else{
            text_finish();
            text.x = MouseEvent.offsetX;
            text.y = MouseEvent.offsetY;
            drawText();
        }
    }
    else if(tool_selected == "rect" || tool_selected == "circle" || tool_selected == "triangle")
        shapeHandler(MouseEvent);
});
canvas.addEventListener("mouseup", function(){
    isDragged = false;
    if(tool_selected != "text")
        imgStackHandler();
});
canvas.addEventListener("mouseleave", function(){
    if(isDragged){
        imgStackHandler();
    }
    isDragged = false;
});

var mouse = {
    start_x: 0,
    start_y: 0,
    end_x: 0,
    end_y: 0
};

canvas.addEventListener("mousemove", function(MouseEvent){
    mouse.end_x = MouseEvent.offsetX;
    mouse.end_y = MouseEvent.offsetY;
    if(isDragged){
        draw();
    }
    mouse.start_x = mouse.end_x;
    mouse.start_y = mouse.end_y;
    // console.log(`start_x=${mouse.start_x}, start_y=${mouse.start_y}`);
    // console.log(`end_x=${mouse.end_x}, end_y=${mouse.end_y}`);
});

var shape = {
    x:"0",
    y:"0",
    radius:"0",
    width:"0",
    height:"0",
};
function shapeHandler(MouseEvent){
    shape.x = MouseEvent.offsetX;
    shape.y = MouseEvent.offsetY;
    console.log(shape.x+", "+shape.y);
}

var ctx = canvas.getContext("2d");

function draw(){
    switch(tool_selected){
        case "brush":
        case "eraser":
        ctx.beginPath();
        ctx.lineCap = "round";
        ctx.moveTo(mouse.start_x, mouse.start_y);
        ctx.lineTo(mouse.end_x, mouse.end_y);
        ctx.stroke();
        break;
        case "circle":
        ctx.putImageData(img_stack[sp], 0, 0);
        shape.radius = Math.sqrt(Math.pow(shape.x-mouse.end_x, 2)+Math.pow(shape.y-mouse.end_y, 2));
        ctx.beginPath();
        ctx.arc(shape.x, shape.y, shape.radius, 0, 2*Math.PI);
        ctx.fill();
        break;
        case "rect":
        ctx.putImageData(img_stack[sp], 0, 0);
        ctx.beginPath();
        ctx.fillRect(shape.x, shape.y, mouse.end_x-shape.x, mouse.end_y-shape.y);
        break;
        case "triangle":
        ctx.putImageData(img_stack[sp], 0, 0);
        ctx.beginPath();
        ctx.moveTo(shape.x, shape.y);
        ctx.lineTo(mouse.end_x, mouse.end_y);
        ctx.lineTo(shape.x-(mouse.end_x-shape.x), shape.y+(mouse.end_y-shape.y));
        ctx.fill();
        break;
    }
}

var fontType_btn = document.getElementById("fontType_btn");
var font1 = document.getElementById("sans-serif");
var font2 = document.getElementById("Times New Roman");
var font3 = document.getElementById("Arial");
var font4 = document.getElementById("Courier New");
var fontSize_btn = document.getElementById("fontSize_btn");
function change_fontType(s){
    fontType_btn.innerText = "Font: "+s;
    text.font_type = s;
    ctx.font = `${text.font_size} ${s}`;
    console.log(s);
    console.log(ctx.font);
}
function change_fontSize(n){
    fontSize_btn.innerText = "Font Size: "+n;
    text.font_size = n+"px";
    ctx.font = n+"px "+text.font_type;
    console.log("FontSize:"+n);
    console.log(ctx.font);
}

var text = {x:"0", y:"0", str:"", ready:false, font_size:"30px", font_type:"sans-serif"}
var text_stack = [];
var text_index = "0";
var imgData_tmp;
ctx.font = "30px sans-serif";
function drawText(){
    ctx.beginPath();
    ctx.moveTo(text.x-25, text.y-10);
    ctx.lineTo(text.x-5, text.y-10);
    ctx.moveTo(text.x-15, text.y-20);
    ctx.lineTo(text.x-3, text.y-10);
    ctx.lineTo(text.x-15, text.y);
    ctx.strokeStyle = "#e1e1d0";
    ctx.lineWidth = 3;
    ctx.lineJoin = "miter";
    ctx.stroke();
    imgData_tmp = ctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "#e1e1d0";
    ctx.fillText("Type something and press ENTER!", text.x, text.y);
    ctx.fillStyle = color;
    text.ready = true;
    console.log(ctx.font);
}
document.addEventListener("keypress", function(e){
    if(tool_selected == "text" && text.ready){
        var key = e.which || e.keyCode;
        console.log(key);
        if(key >= 32 && key < 127){
            text_stack.push(e.key);
            text.str = text_stack.join("");
            ctx.putImageData(imgData_tmp, 0, 0);
            ctx.fillText(text.str, text.x, text.y);
            console.log(text_stack.join());
        }
        else if(key = 13){
            text_finish();
        }
    }
});
document.addEventListener("keydown", function(e){
    if(tool_selected == "text" && text.ready){
        if(e.which == 8 || e.keyCode == 8){
            text_stack.pop();
            if(text_stack.length != 0){
                text.str = text_stack.join("");
                ctx.putImageData(imgData_tmp, 0, 0);
                ctx.fillText(text.str, text.x, text.y);
            }
            else{
                text.str = "";
                ctx.putImageData(imgData_tmp, 0, 0);
                ctx.fillStyle = "#e1e1d0";
                ctx.font = "30px sans-serif";
                ctx.fillText("Type something and press ENTER!", text.x, text.y);
                ctx.fillStyle = color;
            }
        }
    }
});
function text_finish(){
    ctx.putImageData(img_stack[sp], 0, 0);
    ctx.fillText(text.str, text.x, text.y);
    text.str = "";
    text_stack.length = 0;
    text.ready = false;
    imgStackHandler();
}

//reset
btn_reset.addEventListener("click", function(){
    var newImgData = ctx.createImageData(canvas.width, canvas.height);
    ctx.putImageData(newImgData, 0, 0);
    img_stack.length = 0;
    text.str = "";
    text_stack.length = 0;
    text.ready = false;
    btn_undo.disabled = true;
    btn_redo.disabled = true;
    img_stack[0] = newImgData;
    sp = 0;
});

//undo redo
btn_undo.addEventListener("click", undo);
btn_redo.addEventListener("click", redo);

var img_stack = [];
var sp = "0";  // stack pointer
btn_undo.disabled = true;
btn_redo.disabled = true;
img_stack[0] = ctx.createImageData(canvas.width, canvas.height);

function imgStackHandler(){
    while(sp < img_stack.length-1){
        img_stack.pop();
    }
    img_stack.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    sp++;
    if(btn_undo.disabled)
        btn_undo.disabled = false;
    if(sp == img_stack.length-1)
        btn_redo.disabled = true;
}
function undo(){
    if(text.ready){
        ctx.putImageData(img_stack[sp], 0, 0);
        text_stack.length = 0;
        text.str = "";
        text.ready = false;
    }
    else{
        if(sp > 0){
            ctx.putImageData(img_stack[sp-1], 0, 0);
            sp--;
            if(btn_redo.disabled)
                btn_redo.disabled = false;
        }
        if(sp == 0){
            btn_undo.disabled = true;
        }
    }
}
function redo(){
    if(text.ready){
        text_stack.length = 0;
        text.str = "";
        text.ready = false;
    }
    if(sp < img_stack.length-1){
        ctx.putImageData(img_stack[sp+1], 0, 0);
        sp++;
        if(btn_undo.disabled)
            btn_undo.disabled = false;
    }
    if(sp == img_stack.length-1){
        btn_redo.disabled = true;
    }
}

btn_download.addEventListener("click", function(){
    btn_download.href = canvas.toDataURL();
});

function readImage(){
    if ( this.files && this.files[0] ){
        var FR= new FileReader();
        FR.onload = function(e) {
           var img = new Image();
           img.addEventListener("load", function() {
             ctx.drawImage(img, 0, 0);
           });
           img.src = e.target.result;
        };
        FR.readAsDataURL( this.files[0] );
    }
    imgStackHandler();
}
btn_upload.addEventListener("change", readImage);