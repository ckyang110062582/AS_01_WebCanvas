# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* 所有要求之功能皆完成!
* 所有功能的按鈕都有標示功能名稱
* tool bar左上角的顏色按鈕可以選擇顏色
    * 此顏色會套用在筆刷/文字/圖形
* tool bar右上角的slider可以調整筆刷/橡皮擦大小
* 在無法undo/redo的狀態下 undo/redo按鈕會是disabled狀態
* 筆刷和橡皮擦使用方法相同：鼠標在畫布上拖曳即可
    * 如果看不出來顏色有被擦掉，可以將橡皮擦大小調大(一樣使用右上角的slider)
* text功能可以在canvas上"直接"輸入文字
    * 點擊"Text"按鈕後就可以點擊畫布上的任意位置設置文字起始位置
    * 出現"→Type something and press ENTER"即可透過鍵盤輸入文字
    * 使用者可以即時看見文字在畫布上的效果
    * 支援backspace刪除文字
    * 輸入完畢按下"enter"就可以將文字畫在畫布上
    * 點擊畫布其他地方或切換工具亦有"enter"的效果
* 點擊圖形工具即可在畫布上劃出圖形
    * 點選畫布任意位置後拖曳滑鼠
    * 放開滑鼠左鍵後才會確定圖形
* 點擊"Download"即可將畫布作為png檔保存到本地端
* reset重設畫布：重設後即無法undo/redo

